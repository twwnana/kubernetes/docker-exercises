FROM gradle:7.6.1-jdk17-jammy

ENV DB_USER=${DB_USER_USERNAME} \
  DB_PWD=${DB_USER_PASSWORD} \
  DB_SERVER=${DB_ENDPOINT} \
  DB_NAME=${DB_NAME}

RUN mkdir -p /home/docker-excercise

COPY . /home/docker-excercise
WORKDIR /home/docker-excercise

RUN gradle init && gradle build

WORKDIR /home/docker-excercise/build/libs

CMD ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]